# Itinerary Assistant v3

## Hosting URL
  ---

* [https://itinerary-assistant-app.firebaseapp.com](https://itinerary-assistant-app.firebaseapp.com)
* [https://itinerary-assistant-app.web.app](https://itinerary-assistant-app.web.app)

## IDE Extensions
* GitLens
* Markdown All in One
* SCSS Formatter

## Customized Shortcuts

| Action           | Keybinding         |
| ---------------- | ------------------ |
| Save All         | `ctrl + shift + s` |
| Toggle Word Wrap | `ctrl + shift + z` |

## Commit Detail
  ---
| Commit Message      | Date       | Description                         |
| ------------------- | ---------- | ----------------------------------- |
| **IAv3 Master 001** | 2020-04-11 | [IAv3 Master 001](#iav3-master-001) |
| **IAv3-master-002** | 2020-04-12 | [IAv3-master-002](#iav3-master-002) |


## Commit Description
  ---
#### IAv3 Master 001
1. Initialization of `README.md`

#### IAv3-master-002
1. Modified `.gitignore`

<div  style="display: flex; justify-content: center; align-items: center; position: relative;">
  <div style="display: flex; flex: 1; height: 1px; border-bottom: 1px dashed;"></div>
  <div style="text-align: center; font-style: italic; margin: 0px 8px;"><span>fin</span></div>
  <div style="display: flex; flex: 1; height: 1px; border-bottom: 1px dashed;"></div>
</div>